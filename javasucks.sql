drop database javasucks;

create database javasucks;
use javasucks;

create table funcionarios(
	id bigint auto_increment primary key,
    nome varchar(255),
    dataNascimento date,
    cpf char(3) unique,
    email varchar(255) unique,
    senha varchar(255),
    telefone char(8),
    tipo long);
	## Tipos...alter