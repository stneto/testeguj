drop database hospital;

create database hospital;
use hospital;

create table medico(
	id bigint auto_increment primary key,
	nome varchar(255),
    email varchar(255) not null unique,
    senha varchar(20) not null,
    cpf char(3) not null unique,
    telefone char(8),
    crm varchar(4) not null unique,
    especialidade varchar(255),
    dataNascimento date);
    
insert into medico(nome, email, senha, cpf, telefone, crm, especialidade, dataNascimento) values
	("Missiele Victor","missi","123","123","12345678","1234","Clínico geral",'1990-01-01'),
	("José Félix", "felix", "123","124","12345678", "1235", "Odonto", '1990-01-01');
     
create table paciente(
	id bigint auto_increment primary key,
    nome varchar(255),
    email varchar(255) not null unique,
    senha varchar(20) not null,
    cpf char(3) not null unique,
    telefone char(8),
    dataNascimento date,
    planoSaude boolean);
    
insert into paciente(nome, email, senha, cpf, telefone, dataNascimento, planoSaude) values
	("Germano","stneto","123","125","12345678",'1990-01-01', true),
    ("Luciana Karem", "lu", "123", "126", "12345678", '1990-01-01', true);
    
create table consultas(
	id bigint auto_increment primary key,
    cpf varchar(3),
    crm varchar(4),
    dataCon date,
    foreign key (crm) references medico(crm),
    foreign key (cpf) references paciente(cpf)
    );
    
insert into consultas(cpf, crm, dataCon) values
	(125,"1234",'2018-02-05'),
    (125,"1234",'2018-01-18'),
    (126,"1235",'2018-02-10'),
    (126,"1235",'2018-02-25');

    
select * from consultas where dataCon < curdate();    